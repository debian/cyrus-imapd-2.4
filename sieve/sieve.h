/* A Bison parser, made by GNU Bison 2.5.  */

/* Bison interface for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2011 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     NUMBER = 258,
     STRING = 259,
     IF = 260,
     ELSIF = 261,
     ELSE = 262,
     REJCT = 263,
     FILEINTO = 264,
     REDIRECT = 265,
     KEEP = 266,
     STOP = 267,
     DISCARD = 268,
     VACATION = 269,
     REQUIRE = 270,
     SETFLAG = 271,
     ADDFLAG = 272,
     REMOVEFLAG = 273,
     MARK = 274,
     UNMARK = 275,
     HASFLAG = 276,
     FLAGS = 277,
     NOTIFY = 278,
     DENOTIFY = 279,
     ANYOF = 280,
     ALLOF = 281,
     EXISTS = 282,
     SFALSE = 283,
     STRUE = 284,
     HEADER = 285,
     NOT = 286,
     SIZE = 287,
     ADDRESS = 288,
     ENVELOPE = 289,
     BODY = 290,
     COMPARATOR = 291,
     IS = 292,
     CONTAINS = 293,
     MATCHES = 294,
     REGEX = 295,
     COUNT = 296,
     VALUE = 297,
     OVER = 298,
     UNDER = 299,
     GT = 300,
     GE = 301,
     LT = 302,
     LE = 303,
     EQ = 304,
     NE = 305,
     ALL = 306,
     LOCALPART = 307,
     DOMAIN = 308,
     USER = 309,
     DETAIL = 310,
     RAW = 311,
     TEXT = 312,
     CONTENT = 313,
     DAYS = 314,
     ADDRESSES = 315,
     SUBJECT = 316,
     FROM = 317,
     HANDLE = 318,
     MIME = 319,
     METHOD = 320,
     ID = 321,
     OPTIONS = 322,
     LOW = 323,
     NORMAL = 324,
     HIGH = 325,
     ANY = 326,
     MESSAGE = 327,
     INCLUDE = 328,
     PERSONAL = 329,
     GLOBAL = 330,
     RETURN = 331,
     COPY = 332
   };
#endif
/* Tokens.  */
#define NUMBER 258
#define STRING 259
#define IF 260
#define ELSIF 261
#define ELSE 262
#define REJCT 263
#define FILEINTO 264
#define REDIRECT 265
#define KEEP 266
#define STOP 267
#define DISCARD 268
#define VACATION 269
#define REQUIRE 270
#define SETFLAG 271
#define ADDFLAG 272
#define REMOVEFLAG 273
#define MARK 274
#define UNMARK 275
#define HASFLAG 276
#define FLAGS 277
#define NOTIFY 278
#define DENOTIFY 279
#define ANYOF 280
#define ALLOF 281
#define EXISTS 282
#define SFALSE 283
#define STRUE 284
#define HEADER 285
#define NOT 286
#define SIZE 287
#define ADDRESS 288
#define ENVELOPE 289
#define BODY 290
#define COMPARATOR 291
#define IS 292
#define CONTAINS 293
#define MATCHES 294
#define REGEX 295
#define COUNT 296
#define VALUE 297
#define OVER 298
#define UNDER 299
#define GT 300
#define GE 301
#define LT 302
#define LE 303
#define EQ 304
#define NE 305
#define ALL 306
#define LOCALPART 307
#define DOMAIN 308
#define USER 309
#define DETAIL 310
#define RAW 311
#define TEXT 312
#define CONTENT 313
#define DAYS 314
#define ADDRESSES 315
#define SUBJECT 316
#define FROM 317
#define HANDLE 318
#define MIME 319
#define METHOD 320
#define ID 321
#define OPTIONS 322
#define LOW 323
#define NORMAL 324
#define HIGH 325
#define ANY 326
#define MESSAGE 327
#define INCLUDE 328
#define PERSONAL 329
#define GLOBAL 330
#define RETURN 331
#define COPY 332




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 2068 of yacc.c  */
#line 188 "sieve.y"

    int nval;
    char *sval;
    strarray_t *sl;
    test_t *test;
    testlist_t *testl;
    commandlist_t *cl;
    struct vtags *vtag;
    struct aetags *aetag;
    struct htags *htag;
    struct btags *btag;
    struct ntags *ntag;
    struct dtags *dtag;
    struct ftags *ftag;



/* Line 2068 of yacc.c  */
#line 222 "y.tab.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;


